#!/bin/sh

set -e

if [ "$1" == "secret" ] ; then
  RAILS_ENV=development bundle exec rails secret
  exit 0
fi

if [ "$1" == "rails" ] || [ "$3" == "rails" ] || [ "$1" == "puma" ] || [ "$3" == "puma" ] ; then
  bundle exec rails docker:db
fi

exec "$@"
