# :whale: Cargo

Docker Swarm Container Deployment

It uses [Docker](https://www.docker.com), [Traefik](https://traefik.io) and [LetsEncrypt](https://letsencrypt.org) in the background.

## Prerequisites

For deploying Cargo you need a server which can be reached over the internet.  
Docker must be installed on the server and the Swarm mode must be enabled.  
You should also be familiar with [Docker](https://docs.docker.com) and [Traefik](https://docs.traefik.io).

## Installation

Copy the [docker-compose.yml](docs/docker-compose.yml) and the related config files to the server where you want to install Cargo.

Change the environment variables in the docker-compose.yml. For details please read [Configurations.md](docs/configurations.md).  
Also adjust the Traefik configuration, you find all options in the [Traefik documentation](https://docs.traefik.io/configuration/commons/).

If everything is configured and ready, you just have to run:

```shell
# It creates the docker network
docker network create --opt encrypted --driver overlay --attachable traefik

# And finally to start or update Cargo
docker stack deploy --compose-file docker-compose.yml cargo
```

## Development

### Getting Started

1. Clone the repository
2. Install dependencies and run `bin/setup`
3. Start Cargo with `bundle exec rails server`

### Dependencies

- Git
- Ruby 2.6.5
- Redis
- Postgesql
- MinIO client (optional)

### Running the tests

For running all tests, linters, analysis, ... just type `rake` into your console.

Or if you want to only run the RSpec test, use the following command:

```shell
bundle exec rspec
```

The coverage report (`simplecov`) can be found in the directory [coverage](coverage).
After running the tests just type `open coverage/index.html`
