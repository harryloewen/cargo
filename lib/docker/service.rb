# frozen_string_literal: true

module Docker
  class Service
    include Docker::Base

    def self.all(opts = {}, conn = Docker.connection)
      hashes = Docker::Util.parse_json(conn.get("/services", opts)) || []
      hashes.map { |hash| new(conn, hash) }
    end

    def streaming_logs(opts = {}, &block)
      stack_size = opts.delete(:stack_size) || -1
      wait = opts.delete(:wait)

      msgs = Docker::MessagesStack.new(stack_size)

      excon_params = { response_block: Docker::Util.attach_for(block, msgs, false), idempotent: false }
      excon_params[:read_timeout] = wait unless wait.nil?

      connection.get(path_for(:logs), opts, excon_params)
    rescue Docker::Error::TimeoutError
      nil
    end

  private

    def path_for(resource)
      "/services/#{id}/#{resource}"
    end

    private_class_method :new
  end
end
