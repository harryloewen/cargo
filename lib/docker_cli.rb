# frozen_string_literal: true

require "open3"

module DockerCli
  extend ActiveSupport::Autoload

  autoload :Config
  autoload :Secret
  autoload :Stack

  class << self
    def login(url:, username:, password:)
      stdout, stderr = Open3.capture3("docker", "login", "--username", username, "--password-stdin", url, stdin_data: password)

      [stdout.presence, stderr.presence].compact.join("\n").chomp
    end
  end
end
