# frozen_string_literal: true

module Cargo
  class Configuration
    attr_accessor :stack_prefix

    attr_accessor :active_storage

    attr_accessor :oidc_issuer
    attr_accessor :oidc_host
    attr_accessor :oidc_identifier
    attr_accessor :oidc_secret

    attr_accessor :application_setting_prometheus_host_key
    attr_accessor :application_setting_minio_access_key
    attr_accessor :application_setting_minio_secret_key
    attr_accessor :config_value_key
    attr_accessor :config_review_value_key
    attr_accessor :deployment_template_key
    attr_accessor :external_service_environment_claim_settings_key
    attr_accessor :frontend_auth_basic_users_key
    attr_accessor :service_healthcheck_test_key
    attr_accessor :stack_registry_password_key

    def initialize # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
      self.stack_prefix = config(:stack_prefix, "cargo") # DEPRECATED

      self.active_storage = config(:active_storage, "local").to_sym

      self.oidc_issuer = config(:oidc_issuer) || config(:oidc_host)
      self.oidc_host = config(:oidc_host)
      self.oidc_identifier = config(:oidc_identifier)
      self.oidc_secret = config(:oidc_secret)

      self.application_setting_prometheus_host_key = config_key(:application_setting_prometheus_host_key) # DEPRECATED
      self.application_setting_minio_access_key = config_key(:application_setting_minio_access_key) # DEPRECATED
      self.application_setting_minio_secret_key = config_key(:application_setting_minio_secret_key) # DEPRECATED
      self.config_value_key = config_key(:config_value_key) # DEPRECATED
      self.config_review_value_key = config_key(:config_review_value_key) # DEPRECATED
      self.deployment_template_key = config_key(:deployment_template_key) # DEPRECATED
      self.external_service_environment_claim_settings_key = config_key(:external_service_environment_claim_settings_key) # DEPRECATED
      self.frontend_auth_basic_users_key = config_key(:frontend_auth_basic_users_key) # DEPRECATED
      self.service_healthcheck_test_key = config_key(:service_healthcheck_test_key) # DEPRECATED
      self.stack_registry_password_key = config_key(:stack_registry_password_key) # DEPRECATED
    end

  private

    def config(name, default = nil)
      ENV["CARGO_#{name}".upcase].presence ||
        cargo_config[name.to_s].presence ||
        default
    end

    def config_key(name)
      config(name, Rails.application.secret_key_base)[0..31]
    end

    def cargo_config
      @cargo_config ||= Rails.application.config_for(:cargo) rescue {} # rubocop:disable Style/RescueModifier
    end
  end
end
