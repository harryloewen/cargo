# frozen_string_literal: true

module DockerApi
  class Container
    attr_reader :stack

    delegate :id, :info, to: :@container

    def initialize(stack:, container:)
      @stack = stack
      @container = container
    end

    def name
      info.dig("Labels", "com.docker.swarm.service.name")&.delete_prefix("#{stack.name}_")
    end

    def image
      info.dig("Image").to_s.split("@").first.presence
    end

    def state
      info.dig("State")
    end

    def status
      info.dig("Status")
    end

    def healthy?
      !status.match?(/unhealthy|starting/)
    end

    def ports
      info.fetch("Ports", []).map { |port| port["PrivatePort"].to_i }
    end

    def created_at
      return if info.dig("Created").blank?

      Time.zone.at info.dig("Created")
    end

    def service_id
      info.dig("Labels", "com.docker.swarm.service.id")
    end

    def task_id
      info.dig("Labels", "com.docker.swarm.task.id")
    end
  end
end
