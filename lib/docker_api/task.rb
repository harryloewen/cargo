# frozen_string_literal: true

module DockerApi
  class Task
    attr_reader :stack

    delegate :id, :info, to: :@task

    def initialize(stack:, task:)
      @stack = stack
      @task = task
    end

    def containers
      @containers ||= find_containers
    end

    def error
      info.dig("Status", "Err")
    end

    def state
      info.dig("Status", "State")
    end

    def desired_state
      info.dig("DesiredState")
    end

    def image
      info.dig("Spec", "ContainerSpec", "Image").to_s.split("@").first.presence
    end

    def created_at
      info.dig("CreatedAt")&.to_datetime
    end

    def ok?
      desired_state == state
    end

    def running?
      state == "running"
    end

    def service_id
      info.dig("ServiceID")
    end

  private

    def find_containers
      stack.containers.select { |container| container.task_id == id }
    end
  end
end
