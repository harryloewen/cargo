# frozen_string_literal: true

module DockerApi
  class Service
    attr_reader :stack

    delegate :id, :info, to: :@service

    def initialize(stack:, service:)
      @stack = stack
      @service = service
    end

    def tasks
      @tasks ||= find_tasks
    end

    def containers
      @containers ||= find_containers
    end

    def streaming_logs(since: nil, wait: 30, &block)
      options = { stdout: true, stderr: true, follow: true, since: since, wait: wait }.compact

      @service.streaming_logs(options, &block)
    end

    def name
      info.dig("Spec", "Name")&.delete_prefix("#{stack.name}_")
    end

    def image
      info.dig("Spec", "TaskTemplate", "ContainerSpec", "Image").to_s.split("@").first.presence
    end

    def replicas
      info.dig("Spec", "Mode", "Replicated", "Replicas").to_i
    end

    def ports
      traefik_frontends.map do |name|
        {
          number: info.dig("Spec", "Labels", "traefik.#{name}.port")&.to_i,
          hosts: info.dig("Spec", "Labels", "traefik.#{name}.frontend.rule").split(":", 2)[1].split(",")
        }
      end
    end

    def container_ports
      @container_ports ||= containers.map(&:ports).flatten
    end

    def status
      healthcheck? ? status_with_healtcheck : status_without_healtcheck
    end

    def healthcheck?
      info.dig("Spec", "TaskTemplate", "ContainerSpec", "Healthcheck").present?
    end

    def created_at
      info.dig("CreatedAt")&.to_datetime
    end

    def updated_at
      info.dig("UpdatedAt")&.to_datetime
    end

  private

    def traefik_frontends
      info.dig("Spec", "Labels").map { |key, _| key.split(".", 3)[1] if key.match?(/traefik\.\w+\.port/) }.compact.uniq
    end

    def find_tasks
      stack.tasks.select { |task| task.service_id == id }
    end

    def find_containers
      stack.containers.select { |container| container.service_id == id }
    end

    def status_with_healtcheck
      if containers.any? && containers.all?(&:healthy?)
        :success
      elsif containers.any?(&:healthy?)
        :warning
      else
        :danger
      end
    end

    def status_without_healtcheck
      if replicas <= tasks.count(&:ok?)
        :success
      else
        :danger
      end
    end
  end
end
