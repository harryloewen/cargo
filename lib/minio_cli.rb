# frozen_string_literal: true

module MinioCli
  TARGET = "cargo"

  class Error < StandardError
    attr_reader :context

    def initialize(options = nil)
      if options.is_a? String
        message = options
      elsif options.is_a? Hash
        message = options[:message]
        @context = options[:context]
      end
      super(message)
    end
  end

  class << self
    def make_bucket(name, target: TARGET)
      mc("mb", "#{target}/#{name}")
    end

    def remove_bucket(name, target: TARGET)
      mc("rb", "#{target}/#{name}")
    end

    def add_user(access_key, secret_key, target: TARGET)
      mc("admin", "user", "add", target, access_key, secret_key)
    end

    def remove_user(access_key, target: TARGET)
      mc("admin", "user", "remove", target, access_key)
    end

    def add_policy(name, file, target: TARGET)
      mc("admin", "policy", "add", target, name, file)
    end

    def remove_policy(name, target: TARGET)
      mc("admin", "policy", "remove", target, name)
    end

    def set_policy(name, user, target: TARGET)
      mc("admin", "policy", "set", target, name, "user=#{user}")
    end

    def add_host(host, access_key, secret_key, target: TARGET)
      mc("config", "host", "add", target, host, access_key, secret_key)
    end

    def remove_host(target: TARGET)
      mc("config", "host", "remove", target)
    end

    def info(target: TARGET)
      mc("admin", "info", "server", target)
    end

  private

    def mc(*args)
      output, status = Open3.capture2e("mc", "--json", *args.map(&:to_s))
      output = JSON.parse(output, object_class: ActiveSupport::InheritableOptions)
      return output if status.success?

      raise MinioCli::Error, message: "#{output.error.message} #{output.error.cause&.message}", context: output
    end
  end
end
