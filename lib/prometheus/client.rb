# frozen_string_literal: true

module Prometheus
  class Client
    def query_range(params)
      params[:timeout] ||= 10
      params[:step] ||= 60
      params[:start] = params[:start]&.iso8601
      params[:end] = params[:end]&.iso8601

      agent.get "/api/v1/query_range", params
    end

  private

    def agent
      raise "Prometheus config missing" unless Current.settings.prometheus?

      @agent ||= Faraday.new(url: Current.settings.prometheus_host) do |conn|
        conn.request :json

        conn.response :json, content_type: /\bjson$/
        conn.response :logger, Rails.logger, headers: false, bodies: false

        conn.use :instrumentation

        conn.adapter Faraday.default_adapter
      end
    end
  end
end
