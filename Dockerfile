FROM ruby:2.6.6-alpine

RUN apk add --no-cache --update \
  build-base zlib-dev libxml2-dev libxslt-dev tzdata git curl \
  postgresql-dev \
  nodejs yarn \
  docker

COPY --from=minio/mc:latest /usr/bin/mc /usr/bin/mc

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN gem install bundler

ENV NODE_ENV production
ENV RAILS_ENV production
ENV RAILS_SERVE_STATIC_FILES true
ENV RAILS_LOG_TO_STDOUT true

COPY Gemfile Gemfile.lock  ./
RUN bundle config set frozen 'true'; bundle install --jobs $(nproc)

COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile

COPY . ./

COPY --from=registry.gitlab.com/tobi-grins/cargo:latest /usr/src/app/public/assets ./public/assets
COPY --from=registry.gitlab.com/tobi-grins/cargo:latest /usr/src/app/public/packs ./public/packs
RUN bundle exec rails assets:precompile "assets:clean[2]" SECRET_KEY_BASE=1

ARG REVISION=dev
ENV REVISION $REVISION
RUN echo "$REVISION" > REVISION

EXPOSE 3000
ENTRYPOINT ["bin/docker-entrypoint.sh"]
CMD ["bundle", "exec", "puma"]
