# frozen_string_literal: true

require "rails_helper"

RSpec.describe DockerCli::Stack do
  let(:name) { "Name" }
  let(:docker_compose) { { "version" => "2.7" } }

  describe ".deploy" do
    it { expect(described_class.deploy(name: name, docker_compose: docker_compose)).to eq("stdout\nstderr") }

    it do
      described_class.deploy(name: name, docker_compose: docker_compose)
      expect(Open3).to have_received(:capture3).with("docker", "stack", "deploy", "--prune", "--with-registry-auth", "--compose-file", "-", name.downcase, stdin_data: docker_compose.to_yaml)
    end
  end

  describe ".remove" do
    it { expect(described_class.remove(name: name)).to eq("stdout\nstderr") }

    it do
      described_class.remove(name: name)
      expect(Open3).to have_received(:capture3).with("docker", "stack", "rm", name.downcase)
    end
  end
end
