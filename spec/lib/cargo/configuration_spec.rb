# frozen_string_literal: true

require "rails_helper"

RSpec.describe Cargo::Configuration do
  subject(:config) { described_class.new }

  before { allow(Rails.application).to receive(:config_for).and_return({}) }

  it { is_expected.to respond_to(:stack_prefix) }

  it { is_expected.to respond_to(:active_storage) }

  it { is_expected.to respond_to(:oidc_issuer) }
  it { is_expected.to respond_to(:oidc_host) }
  it { is_expected.to respond_to(:oidc_identifier) }
  it { is_expected.to respond_to(:oidc_secret) }

  it { is_expected.to respond_to(:config_value_key) }
  it { is_expected.to respond_to(:config_review_value_key) }
  it { is_expected.to respond_to(:deployment_template_key) }
  it { is_expected.to respond_to(:frontend_auth_basic_users_key) }
  it { is_expected.to respond_to(:service_healthcheck_test_key) }
  it { is_expected.to respond_to(:stack_registry_password_key) }

  it { expect(config.stack_prefix).to eq("cargo") }

  it { expect(config.active_storage).to eq(:local) }

  it { expect(config.oidc_issuer).to be_blank }
  it { expect(config.oidc_host).to be_blank }
  it { expect(config.oidc_identifier).to be_blank }
  it { expect(config.oidc_secret).to be_blank }

  it { expect(config.config_value_key.length).to eq(32) }
  it { expect(config.config_review_value_key.length).to eq(32) }
  it { expect(config.deployment_template_key.length).to eq(32) }
  it { expect(config.frontend_auth_basic_users_key.length).to eq(32) }
  it { expect(config.service_healthcheck_test_key.length).to eq(32) }
  it { expect(config.stack_registry_password_key.length).to eq(32) }
end
