# frozen_string_literal: true

require "rails_helper"

RSpec.describe DockerApi::Stack do
  subject(:stack) { described_class.new(name) }

  let(:name) { "Name" }

  describe "#name" do
    it { expect(stack.name).to eq(name) }
  end

  describe "#services" do
    let(:services) { stack.services }

    it do
      expect(services).to be_kind_of(Array)
      expect(services[0]).to be_instance_of(DockerApi::Service)
    end

    it "catches Excon errors" do
      expect(Docker::Service).to receive(:all).and_raise(Excon::Error)

      expect(services).to be_kind_of(Array)
      expect(services).to be_empty
    end
  end

  describe "#tasks" do
    let(:tasks) { stack.tasks }

    it do
      expect(tasks).to be_kind_of(Array)
      expect(tasks[0]).to be_instance_of(DockerApi::Task)
    end

    it "catches Excon errors" do
      expect(Docker::Task).to receive(:all).and_raise(Excon::Error)

      expect(tasks).to be_kind_of(Array)
      expect(tasks).to be_empty
    end
  end

  describe "#containers" do
    let(:containers) { stack.containers }

    it do
      expect(containers).to be_kind_of(Array)
      expect(containers[0]).to be_instance_of(DockerApi::Container)
    end

    it "catches Excon errors" do
      expect(Docker::Container).to receive(:all).and_raise(Excon::Error)

      expect(containers).to be_kind_of(Array)
      expect(containers).to be_empty
    end
  end
end
