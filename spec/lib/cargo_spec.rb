# frozen_string_literal: true

require "rails_helper"

RSpec.describe Cargo do
  describe ".config" do
    it { expect(described_class.config).to be_instance_of(Cargo::Configuration) }
  end
end
