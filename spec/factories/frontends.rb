# frozen_string_literal: true

FactoryBot.define do
  factory :frontend do
    association :service, factory: :service
    stack { service.stack }

    sequence(:name) { |n| "Name-#{n}" }
    port { rand(0..65_535) }
    hosts { "#{Faker::Internet.domain_name}\n#{Faker::Internet.domain_name}" }
    ignore_wrong_dns_entries { "1" }

    redirect { false }
    auth_basic { false }
    hsts { false }
  end
end
