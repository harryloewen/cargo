# frozen_string_literal: true

FactoryBot.define do
  factory :stack do
    sequence(:name) { |n| "Name-#{n}" }
    sequence(:short_name) { |n| "n#{n}" }
    registry_url { "https://registry.example.com" }
    registry_username { "username" }
    registry_password { "password" }
  end
end
