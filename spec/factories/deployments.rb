# frozen_string_literal: true

FactoryBot.define do
  factory :deployment do
    association :stack, factory: :stack
    association :environment, factory: :environment

    version { Faker::App.semantic_version }
  end
end
