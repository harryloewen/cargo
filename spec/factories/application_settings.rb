# frozen_string_literal: true

FactoryBot.define do
  factory :application_setting do
    allow_creation_of_stacks { false }
    default_production_environment_name { "production" }
    frontend_host_schema { 0 }
    user_default_approved_regex { "" }
    host { "localhost" }
    traefik_network { "traefik" }
    backend_name { "cargo-cargo-web" }
    prometheus { false }
    prometheus_host { "" }
  end
end
