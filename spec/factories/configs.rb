# frozen_string_literal: true

FactoryBot.define do
  factory :config do
    association :stack, factory: :stack
    config_type { "config" }
    sequence(:name) { |n| "Name-#{n}" }
    value { Faker::GreekPhilosophers.quote }
    review_value { Faker::GreekPhilosophers.quote }

    factory :config_secret do
      config_type { "secret" }
    end

    factory :config_environment do
      config_type { "environment" }
    end
  end
end
