# frozen_string_literal: true

RSpec.configure do |config|
  config.before(:suite) do
    OmniAuth.config.test_mode = true
    OmniAuth.config.logger = Rails.logger
  end
end
