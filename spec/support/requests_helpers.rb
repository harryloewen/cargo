# frozen_string_literal: true

module RequestsHelpers
  def json_response
    @json_response ||= begin
      json = JSON.parse(response.body)
      json = json.with_indifferent_access if json.respond_to?(:with_indifferent_access)
      json
    end
  end
end

RSpec.configure do |config|
  config.include RequestsHelpers, type: :request
end
