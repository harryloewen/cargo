# frozen_string_literal: true

require "rails_helper"

RSpec.describe SessionChannel, type: :channel do
  let(:session) { instance_double("TerminalSessionService", stream: "session_test", start: true, closed?: false, write: true, resize: true, close: true) }
  let(:user) { create(:user) }
  let(:stack) { create(:stack, users: [user]) }
  let(:environment) { create(:environment, stack: stack) }

  before do
    stub_connection current_user: user
    allow(TerminalSessionService).to receive(:new).with(environment.docker_containers.first.id, any_args).and_return(session)
  end

  it "rejects when no environment and container is given" do
    subscribe

    expect(subscription).to be_rejected
  end

  it "subscribes to a stream when environment and container is provided" do
    subscribe(environment: environment.id, container: environment.docker_containers.first.id)

    expect(subscription).to be_confirmed
    expect(subscription).to have_stream_from("session_test")
    expect(session).to have_received(:start)
  end

  describe "#write" do
    it "calls write on the service" do
      subscribe(environment: environment.id, container: environment.docker_containers.first.id)

      expect(subscription.write("d" => "data")).to be_truthy
      expect(session).to have_received(:write).with("data")
    end

    it "handles invalid data" do
      subscribe(environment: environment.id, container: environment.docker_containers.first.id)

      expect(subscription.write("data")).to be_nil
      expect(session).not_to have_received(:write)
    end

    it "returns nil if service is already closed" do
      subscribe(environment: environment.id, container: environment.docker_containers.first.id)

      expect(session).to receive(:closed?).and_return(true)

      subscription.write("d" => "data")

      expect(session).not_to have_received(:write)
      expect(session).to have_received(:close)
    end
  end

  describe "#resize" do
    it "calls resize on the service" do
      subscribe(environment: environment.id, container: environment.docker_containers.first.id)

      expect(subscription.resize("cols" => 20, "rows" => "10")).to be_truthy
      expect(session).to have_received(:resize).with(cols: 20, rows: 10)
    end

    it "returns nil if service is already closed" do
      subscribe(environment: environment.id, container: environment.docker_containers.first.id)

      expect(session).to receive(:closed?).and_return(true)

      subscription.resize("cols" => 20, "rows" => "10")

      expect(session).not_to have_received(:resize)
      expect(session).to have_received(:close)
    end
  end
end
