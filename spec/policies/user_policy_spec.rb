# frozen_string_literal: true

require "rails_helper"

RSpec.describe UserPolicy, type: :policy do
  subject { described_class }

  let(:user) { create(:user) }
  let(:admin) { create(:admin) }
  let(:record) { create(:user) }

  permissions :index? do
    it { is_expected.not_to permit(user, User) }
    it { is_expected.to permit(admin, User) }
  end

  permissions :edit?, :update? do
    it { is_expected.not_to permit(user, record) }
    it { is_expected.to permit(admin, record) }
    it { is_expected.not_to permit(admin, admin) }
  end

  permissions :destroy? do
    it { is_expected.not_to permit(user, record) }
    it { is_expected.to permit(admin, record) }
    it { is_expected.not_to permit(admin, admin) }
  end
end
