# frozen_string_literal: true

require "rails_helper"

RSpec.describe StackPolicy, type: :policy do
  subject { described_class }

  let(:user) { create(:user) }
  let(:user_other) { create(:user) }
  let(:admin) { create(:admin) }
  let(:stack) { create(:stack, users: [user]) }

  permissions :index? do
    it { is_expected.to permit(user, Stack) }
    it { is_expected.to permit(user_other, Stack) }
    it { is_expected.to permit(admin, Stack) }
  end

  permissions :show? do
    it { is_expected.to permit(user, stack) }
    it { is_expected.not_to permit(user_other, stack) }
    it { is_expected.to permit(admin, stack) }
    it { is_expected.not_to permit(admin, Stack.new) }
  end

  permissions :new?, :create? do
    context "when Setting#allow_creation_of_stacks is true" do
      before { allow(Current.settings).to receive(:allow_creation_of_stacks).and_return(true) }

      it { is_expected.to permit(user, Stack) }
      it { is_expected.to permit(admin, Stack) }
    end

    context "when Setting#allow_creation_of_stacks is false" do
      before { allow(Current.settings).to receive(:allow_creation_of_stacks).and_return(false) }

      it { is_expected.not_to permit(user, Stack) }
      it { is_expected.to permit(admin, Stack) }
    end
  end

  permissions :edit?, :update? do
    it { is_expected.to permit(user, stack) }
    it { is_expected.not_to permit(user_other, stack) }
    it { is_expected.to permit(admin, stack) }
  end

  permissions :template? do
    it { is_expected.to permit(user, stack) }
    it { is_expected.not_to permit(user_other, stack) }

    context "when stack is not fresh" do
      before { create(:service, stack: stack) }

      it { is_expected.not_to permit(admin, stack) }
    end
  end

  permissions :destroy? do
    context "when Setting#allow_creation_of_stacks is true" do
      before { allow(Current.settings).to receive(:allow_creation_of_stacks).and_return(true) }

      it { is_expected.to permit(user, stack) }
      it { is_expected.not_to permit(user_other, stack) }
      it { is_expected.to permit(admin, stack) }
    end

    context "when Setting#allow_creation_of_stacks is false" do
      before { allow(Current.settings).to receive(:allow_creation_of_stacks).and_return(false) }

      it { is_expected.not_to permit(user, stack) }
      it { is_expected.not_to permit(user_other, stack) }
      it { is_expected.to permit(admin, stack) }
    end
  end
end
