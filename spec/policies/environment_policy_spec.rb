# frozen_string_literal: true

require "rails_helper"

RSpec.describe EnvironmentPolicy, type: :policy do
  subject { described_class }

  let(:user) { create(:user) }
  let(:admin) { create(:admin) }
  let(:environment) { create(:environment) }
  let(:related_environment) { create(:environment, stack: stack) }
  let(:stack) { create(:stack, users: [user]) }

  permissions :show?, :logs?, :terminal?, :restart_service? do
    it { is_expected.not_to permit(user, environment) }
    it { is_expected.to permit(user, related_environment) }

    it { is_expected.to permit(admin, environment) }
    it { is_expected.to permit(admin, related_environment) }
    it { is_expected.not_to permit(admin, Environment.new) }
  end

  permissions :new?, :create? do
    it { is_expected.to permit(user, Environment) }
  end

  permissions :metrics? do
    context "when prometheus is configured" do
      before { allow(Current.settings).to receive(:prometheus?).and_return(true) }

      it { is_expected.not_to permit(user, environment) }
      it { is_expected.to permit(user, related_environment) }

      it { is_expected.to permit(admin, environment) }
      it { is_expected.to permit(admin, related_environment) }
      it { is_expected.not_to permit(admin, Environment.new) }
    end

    context "when prometheus is not configured" do
      before { allow(Current.settings).to receive(:prometheus?).and_return(false) }

      it { is_expected.not_to permit(user, environment) }
      it { is_expected.not_to permit(user, related_environment) }

      it { is_expected.not_to permit(admin, environment) }
      it { is_expected.not_to permit(admin, related_environment) }
      it { is_expected.not_to permit(admin, Environment.new) }
    end
  end

  permissions :destroy? do
    context "when environment is non production" do
      it { is_expected.not_to permit(user, environment) }
      it { is_expected.to permit(user, related_environment) }

      it { is_expected.to permit(admin, environment) }
      it { is_expected.to permit(admin, related_environment) }
      it { is_expected.not_to permit(admin, Environment.new) }
    end

    context "when environment is production" do
      let(:environment) { create(:environment, name: "production") }
      let(:related_environment) { create(:environment, name: "production", stack: stack) }

      it { is_expected.not_to permit(user, environment) }
      it { is_expected.not_to permit(user, related_environment) }

      it { is_expected.not_to permit(admin, environment) }
      it { is_expected.not_to permit(admin, related_environment) }
      it { is_expected.not_to permit(admin, Environment.new) }
    end
  end
end
