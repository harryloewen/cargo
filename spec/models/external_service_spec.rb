# frozen_string_literal: true

require "rails_helper"

RSpec.describe ExternalService, type: :model do
  subject(:external_service) { build(:external_service) }

  it { is_expected.to define_enum_for(:service_type).with_values(%i[minio]) }

  it { is_expected.to have_readonly_attribute(:stack_id) }
  it { is_expected.to have_readonly_attribute(:service_type) }
  it { is_expected.to have_readonly_attribute(:name) }

  it { is_expected.to belong_to(:stack) }
  it { is_expected.to have_many(:external_service_claims).dependent(:restrict_with_error) }
  it { is_expected.to have_many(:external_service_environment_claims).dependent(:nullify) }

  it { is_expected.to validate_presence_of(:service_type) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_uniqueness_of(:name).case_insensitive.scoped_to(:stack_id) }
  it { is_expected.to have_db_index(%i[stack_id name]).unique(true) }
  it { is_expected.to allow_value("a-B-c-1").for(:name) }
  it { is_expected.not_to allow_value("a_B_c-2").for(:name) }
  it { is_expected.not_to allow_value("@").for(:name) }
  it { is_expected.to validate_uniqueness_of(:minio_production_bucket_name).case_insensitive.allow_nil }
  it { is_expected.to validate_length_of(:minio_production_bucket_name).is_at_least(3).is_at_most(63) }
  it { is_expected.to allow_value("abc-def-123").for(:minio_production_bucket_name) }
  it { is_expected.not_to allow_value("abc_def-123").for(:minio_production_bucket_name) }
  it { is_expected.not_to allow_value("abc.def-123").for(:minio_production_bucket_name) }
  it { is_expected.not_to allow_value("Abc-def-123").for(:minio_production_bucket_name) }
  it { is_expected.not_to allow_value("-bc-def-123").for(:minio_production_bucket_name) }
  it { is_expected.to allow_value("").for(:minio_production_bucket_name) }
  it { is_expected.to allow_value(nil).for(:minio_production_bucket_name) }

  describe "minio_production_bucket_name=" do
    it { expect { external_service.minio_production_bucket_name = "" }.to change(external_service, :minio_production_bucket_name).to(nil) }
  end
end
