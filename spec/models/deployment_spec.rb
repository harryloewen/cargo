# frozen_string_literal: true

require "rails_helper"

RSpec.describe Deployment, type: :model do
  subject(:deployment) { create(:deployment) }

  it { is_expected.to serialize(:tags) }

  # it { is_expected.to belong_to(:stack).required }
  it { is_expected.to belong_to(:environment).required }

  it { expect(described_class).to be_attr_encrypted(:template) }

  it { is_expected.not_to allow_value("invalid value").for(:tags) }
  it { is_expected.not_to allow_value("invalid\nvalue:").for(:images) }
  it { is_expected.to allow_value("valid: value").for(:tags) }
  it { is_expected.to allow_value("valid: value").for(:images) }
  it { is_expected.to allow_value(valid: :value).for(:tags) }
  it { is_expected.to allow_value(valid: :value).for(:images) }

  # TODO
  # before_validation :set_stack
  # before_create :set_template
  # after_create :deploy_stack

  it { is_expected.to delegate_method(:name).to(:environment) }
  it { is_expected.to delegate_method(:docker_name).to(:environment) }
  it { is_expected.to delegate_method(:deleted_at).to(:environment) }
  it { is_expected.to delegate_method(:production?).to(:environment) }

  describe "#deploy_stack" do
    subject(:deployment) { build(:deployment) }

    context "when not deleted" do
      before { allow(DockerCli::Stack).to receive(:deploy).and_return("message") }

      it do
        expect(deployment.environment.deployed_at).to be_nil
        expect(deployment.deploy_stack).to be_truthy
        expect(deployment.environment.last_deployment).to be(deployment)
        expect(deployment.environment.deployed_at).not_to be_nil
        expect(deployment.message).to eq("stdout\nstderr\nmessage")
      end
    end

    context "when deleted" do
      subject(:deployment) { build(:deployment, environment: build(:environment, deleted_at: Time.current)) }

      it { expect(deployment.deploy_stack).to be_falsey }
    end
  end

  describe "#tags=" do
    it "normalizes a hash" do
      deployment.tags = { A: :b, "c" => :d }
      expect(deployment.tags).to eq("a" => :b, "c" => :d)
    end

    it "converts a string" do
      deployment.tags = "A: b\nc: d"
      expect(deployment.tags).to eq("a" => "b", "c" => "d")
    end

    it "ignores invalid data" do
      deployment.tags = "invalid"
      expect(deployment.tags).to eq("invalid")
    end
  end

  describe "#tags_yaml" do
    it "ignores blank values" do
      deployment.tags = ""
      expect(deployment.tags_yaml).to eq("")
    end

    it "normalizes a hash" do
      deployment.tags = { A: :b, "c" => :d }
      expect(deployment.tags_yaml).to eq("a: :b\nc: :d")
    end

    it "converts a string" do
      deployment.tags = "A: b\nc: d"
      expect(deployment.tags_yaml).to eq("a: b\nc: d")
    end
  end

  describe "#environment=" do
    it "works like expected" do
      deployment.environment = build(:environment, name: "Test")
      expect(deployment.environment.name).to eq("Test")
    end

    it "converts a string" do
      deployment.environment = "Test"
      expect(deployment.environment.name).to eq("Test")
    end
  end

  describe "#message_short" do
    subject(:deployment) { build(:deployment, message: "Line\n\n  \nLine\nLine\Line") }

    it { expect(deployment.message_short).to eq("Line ...") }
  end

  describe "set stack" do
    subject(:deployment) { create(:deployment, stack: nil) }

    it { expect(deployment.stack).to be(deployment.environment.stack) }
  end

  context "when host name is too long" do
    subject(:deployment) { build(:deployment, environment: environment, stack: stack) }

    let(:stack) { create(:stack) }
    let(:environment) { create(:environment, stack: stack, name: "very-very-very-long-environment-name-that-should-raise-an-exception") }

    before { create(:frontend, service: create(:service, stack: stack)) }

    it "fails silent with an error" do
      expect(deployment.save).to be_falsey

      expect(deployment.errors.full_messages.join).to include(environment.name)
      expect(deployment.errors.full_messages.join).to include("is too long")
    end
  end

  describe "#frontend_hosts" do
    subject(:deployment) { build(:deployment, template: template) }

    let(:template) do
      {
        "services" => {
          "service-a" => {
            "deploy" => {
              "labels" => {
                "traefik.web.frontend.rule" => "Host:example.com"
              }
            }
          },
          "service-b" => {},
          "service-c" => {
            "deploy" => {
              "labels" => {
                "traefik.foo.frontend.rule" => "Host:a.example.com",
                "traefik.bar.frontend.rule" => "Host:b.example.com,c.example.com"
              }
            }
          }
        }
      }
    end

    it { expect(deployment.frontend_hosts).to eq(%w[example.com a.example.com b.example.com c.example.com]) }
  end
end
