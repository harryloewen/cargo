# frozen_string_literal: true

require "rails_helper"

RSpec.describe StackMember, type: :model do
  subject { build(:stack_member) }

  it { is_expected.to belong_to(:stack).required }
  it { is_expected.to belong_to(:user).required }

  it { is_expected.to validate_uniqueness_of(:user_id).case_insensitive.scoped_to(:stack_id) }
end
