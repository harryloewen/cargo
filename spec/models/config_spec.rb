# frozen_string_literal: true

require "rails_helper"

RSpec.describe Config, type: :model do
  subject(:config) { build(:config) }

  it { is_expected.to define_enum_for(:config_type).with_values(%i[environment secret config]) }

  it { is_expected.to have_readonly_attribute(:stack_id) }
  it { is_expected.to have_readonly_attribute(:config_type) }
  it { is_expected.to have_readonly_attribute(:name) }

  it { is_expected.to belong_to(:stack) }
  it { is_expected.to have_many(:config_claims).dependent(:restrict_with_error) }

  it { expect(described_class).to be_attr_encrypted(:value) }
  it { expect(described_class).to be_attr_encrypted(:review_value) }

  it { is_expected.to validate_presence_of(:config_type) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_uniqueness_of(:name).case_insensitive.scoped_to(:stack_id) }
  it { is_expected.to have_db_index(%i[stack_id name]).unique(true) }
  it { is_expected.to allow_value("a-B-c-1").for(:name) }
  it { is_expected.not_to allow_value("a_B_c-2").for(:name) }
  it { is_expected.not_to allow_value("@").for(:name) }

  it { is_expected.to validate_presence_of(:value) }
  it { is_expected.to validate_presence_of(:language) }

  # TODO
  # after_create :docker_create
  # after_update :docker_create
  # after_destroy :docker_remove

  describe "#docker_name" do
    it { expect(config.docker_name).to eq("#{config.stack.docker_name}_#{config.name.downcase}_production_0") }
    it { expect(config.docker_name(production: true)).to eq("#{config.stack.docker_name}_#{config.name.downcase}_production_0") }
    it { expect(config.docker_name(production: false)).to eq("#{config.stack.docker_name}_#{config.name.downcase}_review_0") }
  end

  describe "#formatted_value" do
    subject(:config) { build(:config, value: "Test %{existing} %{missing}") } # rubocop:disable Style/FormatStringToken

    before { create(:config, name: "existing", value: "existing", stack: config.stack) }

    it { expect(config.formatted_value).to eq("Test existing %{missing}") } # rubocop:disable Style/FormatStringToken

    it "raises an error" do
      expect(Kernel).to receive(:format).and_raise(StandardError)
      expect(config.formatted_value).to eq(config.value)
    end
  end

  describe "#formatted_review_value" do
    subject(:config) { create(:config, review_value: "Test %{existing} %{production} %{missing}") } # rubocop:disable Style/FormatStringToken

    before do
      allow_any_instance_of(described_class).to receive(:docker_create).and_return(true)

      create(:config, name: "existing", review_value: "existing", stack: config.stack)
      create(:config, name: "production", value: "production", review_value: nil, stack: config.stack)
    end

    it { expect(config.formatted_review_value).to eq("Test existing production %{missing}") } # rubocop:disable Style/FormatStringToken

    it "raises an error" do
      expect(Kernel).to receive(:format).and_raise(StandardError)
      expect(config.formatted_review_value).to eq(config.review_value)
    end
  end

  describe "#docker_create" do
    context "when config" do
      it do
        expect(DockerCli::Config).to receive(:create).with(name: config.docker_name, value: config.formatted_value)
        expect(DockerCli::Config).to receive(:create).with(name: config.docker_name(production: false), value: config.formatted_review_value)
        expect(DockerCli::Secret).not_to receive(:create)
        expect(config.docker_create).to be_nil
      end
    end

    context "when environment" do
      subject(:config) { build(:config_environment) }

      it do
        expect(DockerCli::Config).not_to receive(:create)
        expect(DockerCli::Secret).not_to receive(:create)
        expect(config.docker_create).to be_nil
      end
    end

    context "when secret" do
      subject(:config) { build(:config_secret) }

      it do
        expect(DockerCli::Config).not_to receive(:create)
        expect(DockerCli::Secret).to receive(:create).with(name: config.docker_name, value: config.formatted_value)
        expect(DockerCli::Secret).to receive(:create).with(name: config.docker_name(production: false), value: config.formatted_review_value)
        expect(config.docker_create).to be_nil
      end
    end
  end

  describe "#docker_remove" do
    context "when config" do
      it do
        expect(DockerCli::Config).to receive(:remove).with(name: config.docker_name_base)
        expect(DockerCli::Secret).not_to receive(:remove)
        expect(config.docker_remove).to be_nil
      end
    end

    context "when environment" do
      subject(:config) { build(:config_environment) }

      it do
        expect(DockerCli::Config).not_to receive(:remove)
        expect(DockerCli::Secret).not_to receive(:remove)
        expect(config.docker_remove).to be_nil
      end
    end

    context "when secret" do
      subject(:config) { build(:config_secret) }

      it do
        expect(DockerCli::Config).not_to receive(:remove)
        expect(DockerCli::Secret).to receive(:remove).with(name: config.docker_name_base)
        expect(config.docker_remove).to be_nil
      end
    end
  end
end
