# frozen_string_literal: true

require "rails_helper"

RSpec.describe ExternalServices::MinioService do
  subject(:service) { described_class.new(ApplicationSetting.new(minio_host: :mocked_host, minio_access_key: :mocked_key, minio_secret_key: :mocked_secret)) }

  describe "#try_connection" do
    it do
      expect(MinioCli).to receive(:info).and_call_original

      expect(service.try_connection).to eq("success")
    end

    it do
      expect(MinioCli).to receive(:info).and_raise(MinioCli::Error, "error message")

      expect(service.try_connection).to eq("error message")
    end
  end

  describe "#create_bucket" do
    it do
      expect(MinioCli).to receive(:make_bucket).and_call_original
      expect(MinioCli).to receive(:add_user).and_call_original
      expect(MinioCli).to receive(:add_policy).and_call_original
      expect(MinioCli).to receive(:set_policy).and_call_original

      expect(service.create_bucket("bucket-name")).to include(bucket_name: "bucket-name", access_key: be_kind_of(String).and(match(/[a-z0-9]{32}/i)), secret_key: be_kind_of(String).and(match(/[a-z0-9]{64}/i)), policy_name: "bucket-name-policy")
    end
  end

  describe "#remove_bucket" do
    it do
      expect(MinioCli).to receive(:remove_user).and_call_original
      expect(MinioCli).to receive(:remove_policy).and_call_original
      expect(MinioCli).to receive(:remove_bucket).and_call_original

      service.remove_bucket access_key: "mocked_access_key", bucket_name: "mocked_bucket_name", policy_name: "mocked_policy_name"
    end
  end
end
