# frozen_string_literal: true

require "rails_helper"

RSpec.describe DockerComposeService do
  subject { described_class.new(deployment: deployment) }

  let!(:stack) { create(:stack, name: "stack", short_name: "s") }
  let!(:service) { create(:service, stack: stack, name: "service") }
  let!(:config) { create(:config_claim, service: service, target: "config", config: create(:config, name: "config", stack: stack, value: "value", review_value: "review_value")) }
  let!(:config_environment) { create(:config_claim, service: service, target: "environment", config: create(:config_environment, name: "environment", stack: stack, value: "value", review_value: "review_value")) }
  let!(:config_secret) { create(:config_claim, service: service, target: "secret", config: create(:config_secret, name: "secret", stack: stack, value: "value", review_value: "review_value")) }
  let!(:volume) { create(:volume_claim, service: service, target: "volume", volume: create(:volume, name: "volume")) }
  let!(:frontend) { create(:frontend, service: service, name: "frontend", port: 3000) }
  let!(:environment) { create(:environment, stack: stack, name: "environment") }
  let!(:deployment) { create(:deployment, stack: stack, environment: environment) }

  describe "#content" do
    let(:content) { subject.content }
    let(:example) do
      yaml = <<~YAML
        ---
        version: '3.7'
        services:
          service:
            image: service:latest
            volumes:
            - volume:volume
            networks:
            - overlay
            - traefik
            configs:
            - source: cargo_s_config_review_0
              target: config
            environment:
              CARGO_ENV: environment
              environment: review_value
              CARGO_FRONTEND_FRONTEND_HOSTS: environment.s.localhost
            secrets:
            - source: cargo_s_secret_review_0
              target: secret
            deploy:
              replicas: 1
              rollback_config:
                parallelism: 1
                delay: 60s
                failure_action: continue
                monitor: 60s
                order: start-first
              update_config:
                parallelism: 1
                delay: 60s
                failure_action: rollback
                monitor: 60s
                order: start-first
              restart_policy:
                condition: any
                delay: 60s
                window: 60s
              labels:
                traefik.enable: 'true'
                traefik.frontend.port: 3000
                traefik.frontend.frontend.rule: Host:environment.s.localhost
                traefik.frontend.frontend.passHostHeader: 'true'
                traefik.frontend.frontend.errors.cargo.backend: cargo-cargo-web
                traefik.frontend.frontend.errors.cargo.status: '502,503,504'
                traefik.frontend.frontend.errors.cargo.query: "/errors/service_unavailable"
        configs:
          cargo_s_config_review_0:
            external: true
        secrets:
          cargo_s_secret_review_0:
            external: true
        volumes:
          volume:
        networks:
          traefik:
            external: true
          overlay:
            driver: overlay
            driver_opts:
              encrypted: 'true'

      YAML
      YAML.safe_load(yaml).to_yaml
    end

    it { expect(content).to be_a(Hash) }

    it "matches the example" do
      expect(content.to_yaml).to eq(example)
    end
  end
end
