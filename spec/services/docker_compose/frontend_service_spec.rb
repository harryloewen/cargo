# frozen_string_literal: true

require "rails_helper"

RSpec.describe DockerCompose::FrontendService do
  subject(:service) { described_class.new(frontend: frontend, deployment: deployment) }

  let(:frontend) { create(:frontend) }
  let(:environment) { create(:environment) }
  let(:deployment) { create(:deployment, environment: environment) }

  describe "#labels" do
    let(:result) { service.labels }

    describe "default" do
      it { expect(result).to be_hash_with("traefik.enable", "true") }
      it { expect(result).to be_hash_with("traefik.*.frontend.rule", "Host:*.localhost") }
      it { expect(result).to be_hash_with("traefik.*.port", frontend.port) }

      context "when production" do
        let(:environment) { build(:environment, name: "production") }
        let(:frontend) { create(:frontend, hosts: "production.example.com") }

        it { expect(result).to be_hash_with("traefik.*.frontend.rule", "Host:production.example.com") }
      end

      it "uses subdomain_stack for hosts" do
        expect(service).to receive(:subdomain_stack).and_call_original
        expect(deployment.stack).to receive_message_chain("frontends.count") { 1 }

        expect(result).to be_hash_with("traefik.*.frontend.rule", "Host:*.localhost")
        expect(result["traefik.#{frontend.name}.frontend.rule".downcase.to_sym].split(".").count).to eq(3)
      end

      it "uses subdomain_frontend for hosts" do
        expect(service).to receive(:subdomain_frontend).and_call_original
        allow(deployment.stack).to receive(:frontends).and_return(double("frontends", count: 2, where: double("where", count: 1)))
        expect(result).to be_hash_with("traefik.*.frontend.rule", "Host:*.localhost")
        expect(result["traefik.#{frontend.name}.frontend.rule".downcase.to_sym].split(".").count).to eq(4)
      end

      it "generates single_subdomain if frontend_host_schema flag is set" do
        Current.settings.frontend_host_schema = 1
        expect(service).to receive(:subdomain).and_call_original
        expect(result).to be_hash_with("traefik.*.frontend.rule", "Host:*.localhost")
        expect(result["traefik.#{frontend.name}.frontend.rule".downcase.to_sym].split(".").count).to eq(2)
        expect(result["traefik.#{frontend.name}.frontend.rule".downcase.to_sym]).to eq "Host:#{deployment.stack.short_name}-#{frontend.name}-#{deployment.name}.localhost"
      end
    end

    describe "auth_basic" do
      let(:frontend) { create(:frontend, auth_basic: true, auth_basic_users: "test: test") }

      it { expect(result).to be_hash_with("traefik.enable", "true") }
      it { expect(result).to be_hash_with("traefik.*.frontend.auth.basic.users", "test:*") }
      it { expect(result).to be_hash_with("traefik.*.frontend.auth.basic.removeHeader", "true") }

      it "catches exceptions" do
        expect(BCrypt::Password).to receive(:create).and_raise(StandardError)

        expect(result).to be_hash_with("traefik.*.frontend.auth.basic.users", "")
      end
    end

    describe "redirect" do
      let(:frontend) { create(:frontend, redirect: true, redirect_regex: "^https://www\\.(.*)", redirect_replacement: "https://$$1") }

      it { expect(result).to be_hash_with("traefik.enable", "true") }
      it { expect(result).to be_hash_with("traefik.*.frontend.redirect.regex", "^https://www\\.(.*)") }
      it { expect(result).to be_hash_with("traefik.*.frontend.redirect.replacement", "https://$$1") }
      it { expect(result).to be_hash_with("traefik.*.frontend.redirect.permanent", "true") }
    end

    describe "custom_headers" do
      let(:frontend) { create(:frontend, request_headers: "a: b", response_headers: "c: d\ne: f") }

      it { expect(result).to be_hash_with("traefik.enable", "true") }
      it { expect(result).to be_hash_with("traefik.*.frontend.customRequestHeaders", "a:b") }
      it { expect(result).to be_hash_with("traefik.*.frontend.customResponseHeaders", "c:d||e:f") }

      it "catches exceptions" do
        expect(YAML).to receive(:safe_load).twice.and_raise(Psych::SyntaxError)
        expect(result).not_to be_hash_with("traefik.*.frontend.customRequestHeaders", "*")
        expect(result).not_to be_hash_with("traefik.*.frontend.customResponseHeaders", "*")
      end
    end

    describe "hsts" do
      let(:environment) { build(:environment, name: "production") }
      let(:frontend) { create(:frontend, hsts: true, response_headers: "c: d\ne: f") }

      it { expect(result).to be_hash_with("traefik.enable", "true") }
      it { expect(result).to be_hash_with("traefik.*.frontend.headers.forceSTSHeader", "true") }
      it { expect(result).to be_hash_with("traefik.*.frontend.headers.STSSeconds", "15552000") }
      it { expect(result).to be_hash_with("traefik.*.frontend.headers.STSIncludeSubdomains", "false") }
    end
  end

  describe "#environment" do
    it { expect(service.environment).to be_hash_with("CARGO_FRONTEND_*_HOSTS", "*") }
  end
end
