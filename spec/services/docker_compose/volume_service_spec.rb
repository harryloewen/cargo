# frozen_string_literal: true

require "rails_helper"

RSpec.describe DockerCompose::VolumeService do
  subject(:volume) { described_class.new(volume_claim: volume_claim) }

  let(:volume_claim) { build(:volume_claim) }

  describe "#service_claim" do
    it { expect(volume.service_claim).to eq("#{volume_claim.volume.name}:#{volume_claim.target}".downcase) }

    context "when readonly" do
      let(:volume_claim) { build(:volume_claim, read_only: true) }

      it { expect(volume.service_claim).to eq("#{volume_claim.volume.name}:#{volume_claim.target}:ro".downcase) }
    end
  end

  describe "#stack_claim" do
    it { expect(volume.stack_claim).to eq(volume_claim.volume.name.downcase => nil) }
  end
end
