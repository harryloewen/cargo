# frozen_string_literal: true

require "rails_helper"

RSpec.describe DockerCompose::ExternalServiceService do
  subject { described_class.new(external_service_claim: external_service_claim, deployment: deployment) }

  let(:external_service) { create(:external_service) }
  let(:external_service_claim) { create(:external_service_claim, external_service: external_service) }
  let(:environment) { create(:environment, name: "production") }
  let(:deployment) { create(:deployment, environment: environment) }

  describe "#environment" do
    let(:result) { subject.environment }

    context "when minio is enabled" do
      before { allow(Current.settings).to receive(:minio?).and_return(true) }

      it { expect(result).to be_hash_with("*_HOST") }
      it { expect(result).to be_hash_with("*_BUCKET", "minio-production-bucket-*") }
      it { expect(result).to be_hash_with("*_ACCESS_KEY", "*") }
      it { expect(result).to be_hash_with("*_SECRET_KEY", "*") }
    end

    context "when minio is disabled" do
      before { allow(Current.settings).to receive(:minio?).and_return(false) }

      it { expect(result).to eq({}) }
    end

    context "when service_type is not minio" do
      before do
        allow(external_service).to receive(:minio?).and_return(false)
        allow(Current.settings).to receive(:minio?).and_return(true)
      end

      it { expect(result).to eq({}) }
    end
  end
end
