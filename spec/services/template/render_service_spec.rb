# frozen_string_literal: true

require "rails_helper"

RSpec.describe Template::RenderService do
  subject(:service) { described_class.new(file: file, stack: stack, inputs: inputs) }

  let(:file) { Rails.root.join("spec/support/template.yml") }
  let(:stack) { create(:stack) }
  let(:inputs) { OpenStruct.new(image: "nginx:lastest") }

  it { is_expected.to respond_to(:stack) }
  it { is_expected.to respond_to(:inputs) }

  describe "#render" do
    it { expect(service.render).to be_kind_of(Hash) }
    it { expect(service.render.to_s).to include(inputs.image) }
  end
end
