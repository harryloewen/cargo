# frozen_string_literal: true

require "rails_helper"

RSpec.describe Api::V1::StacksController, type: :request do
  let(:stack) { create(:stack) }

  describe "#deploy" do
    context "when unauthenticated" do
      it { expect { post deploy_api_v1_stack_path(stack) }.to raise_error(ActiveRecord::RecordNotFound) }
    end

    context "when authenticated" do
      before { post deploy_api_v1_stack_path(stack), params: params, headers: { "X-Token": stack.token } }

      context "when environment is missing" do
        let(:params) { {} }

        it { expect(response).to have_http_status(:unprocessable_entity) }
        it { expect(json_response[:status]).to eq("failed") }
      end

      context "when environment is not missing" do
        let(:params) { { environment: "test" } }

        it { expect(response).to have_http_status(:created) }
        it { expect(json_response[:status]).to eq("created") }
      end
    end
  end

  describe "#remove" do
    let(:environment) { create(:environment, stack: stack) }

    context "when unauthenticated" do
      it { expect { delete remove_api_v1_stack_path(stack) }.to raise_error(ActiveRecord::RecordNotFound) }
    end

    context "when environment is missing" do
      before { delete remove_api_v1_stack_path(stack), headers: { "X-Token": stack.token } }

      it { expect(response).to have_http_status(:unprocessable_entity) }
      it { expect(json_response[:status]).to eq("failed") }
    end

    context "when environment is production" do
      before do
        allow_any_instance_of(Environment).to receive(:production?).and_return(true)
        delete remove_api_v1_stack_path(stack), params: { environment: environment.name }, headers: { "X-Token": stack.token }
      end

      it { expect(response).to have_http_status(:unprocessable_entity) }
      it { expect(json_response[:status]).to eq("failed") }
    end

    context "when environment is not production" do
      before { delete remove_api_v1_stack_path(stack), params: { environment: environment.name }, headers: { "X-Token": stack.token } }

      it { expect(response).to have_http_status(:ok) }
      it { expect(json_response[:status]).to eq("stopped") }
    end

    context "when remove failed" do
      before do
        expect_any_instance_of(Environment).to receive(:remove_stack).and_return(false)
        delete remove_api_v1_stack_path(stack), params: { environment: environment.name }, headers: { "X-Token": stack.token }
      end

      it { expect(response).to have_http_status(:internal_server_error) }
      it { expect(json_response[:status]).to eq("failed") }
    end
  end
end
