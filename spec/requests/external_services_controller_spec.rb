# frozen_string_literal: true

require "rails_helper"

RSpec.describe ExternalServicesController, type: :request do
  before do
    allow(User).to receive(:find_by).and_return(user)
    allow_any_instance_of(ApplicationSetting).to receive(:external_services?).and_return(true) # rubocop:disable RSpec/AnyInstance
  end

  let(:user) { create(:user) }
  let(:stack) { create(:stack, users: [user]) }

  describe "#index" do
    it "contains the external_service" do
      external_service = create(:external_service, stack: stack)

      get stack_external_services_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(external_service.name)
      expect(response.body).to include(external_service_path(external_service))
    end

    it "contains the link to create a new external_service" do
      get stack_external_services_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(new_stack_external_service_path(stack))
    end
  end

  describe "#show" do
    let(:external_service) { create(:external_service, stack: stack) }

    it "shows the external_service" do
      get external_service_path(external_service)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(external_service.name)
    end

    it "contains the link to edit the external_service" do
      get external_service_path(external_service)

      expect(response).to have_http_status(:success)
      expect(response.body).to include(edit_external_service_path(external_service))
    end
  end

  describe "#new" do
    it "shows the form to create a external_service" do
      get new_stack_external_service_path(stack)

      expect(response).to have_http_status(:success)
      expect(response.body).to include("New external service")
      expect(response.body).to include("Create External service")
      expect(response.body).to include(%(name="external_service[name]"))
    end
  end

  describe "#create" do
    it "creates a external_service" do
      expect do
        post stack_external_services_path(stack), params: { external_service: { name: "Test", value: "Value" } }
      end.to change(ExternalService, :count).by(1)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(%r{external_services/[\w\-]{36}})
    end

    it "renders validation errors" do
      post stack_external_services_path(stack), params: { external_service: { name: "" } }

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Please review the problems below:")
      expect(response.body).to include("Name can&#39;t be blank")
    end
  end

  describe "#edit" do
    let(:external_service) { create(:external_service, stack: stack) }

    it "shows the form to update a external_service" do
      get edit_external_service_path(external_service)

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Editing external service")
      expect(response.body).to include(%(name="external_service[name]"))
      expect(response.body).to include("Update External service")
    end
  end

  describe "#update" do
    let(:external_service) { create(:external_service, stack: stack) }

    it "updates the external_service" do
      expect do
        put external_service_path(external_service), params: { external_service: { minio_production_bucket_name: "changed-value" } }
      end.to change { external_service.reload.minio_production_bucket_name }.from(external_service.minio_production_bucket_name).to("changed-value")

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(external_service_path(external_service))
    end

    it "renders validation errors" do
      put external_service_path(external_service), params: { external_service: { name: "" } }

      expect(response).to have_http_status(:success)
      expect(response.body).to include("Please review the problems below:")
      expect(response.body).to include("Name can&#39;t be blank")
    end
  end

  describe "#destroy" do
    let(:external_service) { create(:external_service, stack: stack) }

    it "destroys the external_service" do
      delete external_service_path(external_service)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(stack_external_services_path(stack))
    end

    it "does not destroy the external_service" do
      expect_any_instance_of(ExternalService).to receive(:destroy).and_return(false) # rubocop:disable RSpec/AnyInstance

      delete external_service_path(external_service)

      expect(response).to have_http_status(:found)
      expect(response).to redirect_to(external_service_path(external_service))
    end
  end
end
