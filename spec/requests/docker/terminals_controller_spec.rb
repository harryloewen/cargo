# frozen_string_literal: true

require "rails_helper"

RSpec.describe Docker::TerminalsController, type: :request do
  before { allow(User).to receive(:find_by).and_return(user) }

  let(:user) { create(:user) }
  let(:stack) { create(:stack, users: [user]) }
  let(:environment) { create(:environment, stack: stack) }

  describe "#index" do
    it "shows terminal" do
      get environment_container_terminal_path(environment, "b0e1ddbcc140b88f485f13e4e1d594fc9606195e2573a6dba1b1f80882074d7c")

      expect(response).to have_http_status(:success)
      expect(response.body).to include(environment.name)
      expect(response.body).to include("Terminal")
    end
  end
end
