# frozen_string_literal: true

require "raven/breadcrumbs/logger"

Raven.configure do |config|
  config.environments = %w[staging production]
  config.sanitize_fields = Rails.application.config.filter_parameters.map(&:to_s)
  config.silence_ready = true
  config.rails_activesupport_breadcrumbs = true
  config.release = Rails.configuration.version
  config.async = lambda do |event|
    Thread.new { Raven.send_event(event) }
  end
end
