# frozen_string_literal: true

Rails.application.routes.draw do
  match "(*path)", to: "errors#catch_all", via: :all, constraints: ->(request) { request.headers["X-CATCHALL"].to_s.casecmp?("traefik") }
  resources :errors, only: [] do
    match :not_found, on: :collection, via: :all
    match :service_unavailable, on: :collection, via: :all
  end

  resources :stacks do
    resources :services, shallow: true do
      post :duplicate, on: :member
    end
    resources :configs, shallow: true
    resources :external_services, shallow: true
    resources :frontends, shallow: true
    resources :volumes, shallow: true

    resources :environments, only: %i[new create]
    resources :deployments, shallow: true, only: %i[index new create show]

    get "templates/:name", action: :new, controller: :templates, as: :new_template
    post "templates", action: :create, controller: :templates
  end

  resources :environments, only: %i[show destroy] do
    scope module: :docker do
      resources :services, only: %i[] do
        post :restart, on: :member
        resources :logs, only: %i[index] do
          get :stream, on: :collection
        end
      end
      resources :containers, only: %i[] do
        resource :terminal, only: %i[show]
      end
    end
    resources :services, only: [] do
      resources :metrics, only: [] do
        get :requests, on: :collection
        get :response_time, on: :collection
      end
    end
  end

  resources :users, only: %i[index update destroy]
  resource :application_setting, only: %i[show update]

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :stacks, only: [] do
        post :deploy, on: :member
        delete :remove, on: :member
      end
    end
  end

  get "/auth/:provider/callback", to: "sessions#create", as: :sessions_create
  get "/auth/failure", to: "sessions#failure", as: :sessions_failure

  root "stacks#index"
end
