# frozen_string_literal: true

class DeploymentsController < ApplicationController
  before_action :set_stack, only: %i[index new create]
  before_action :set_deployment, only: %i[show]

  def index
    authorize Deployment, :index?

    @environments = @stack.environments.not_deleted.ordered
  end

  def new # rubocop:disable Metrics/AbcSize
    @deployment = @stack.deployments.new
    authorize @deployment, :create?

    environment = @stack.environments.not_deleted.find_by(name: params[:environment])
    return if environment.blank?

    @deployment.environment = environment

    last_deployment = environment.deployments.ordered.first
    return if last_deployment.blank?

    @deployment.version = last_deployment.version
    @deployment.tags = last_deployment.tags
    @deployment.images = last_deployment.images
  end

  def show
    authorize @deployment, :show?
  end

  def create
    @deployment = @stack.deployments.new(deployment_params)

    authorize @deployment, :create?

    if @deployment.save
      redirect_to [@deployment.stack, :deployments], flash: { success: t(".success") }
    else
      render :new
    end
  end

private

  def set_stack
    @stack = Stack.find(params[:stack_id])

    authorize @stack, :show?
  end

  def set_deployment
    @deployment = Deployment.find(params[:id])
    @stack = @deployment.stack

    authorize @stack, :show?
  end

  def deployment_params
    params.require(:deployment).permit(:environment_id, :version, :tags, :images)
  end
end
