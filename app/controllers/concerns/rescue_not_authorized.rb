# frozen_string_literal: true

module RescueNotAuthorized
  extend ActiveSupport::Concern

  included do
    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  end

private

  def user_not_authorized
    respond_to do |format|
      format.html { redirect_to user_not_authorized_path, alert: t(:user_not_authorized) }
      format.json { render json: { status: "forbidden" }, status: :forbidden }
    end
  end

  def user_not_authorized_path
    request.referer || root_path
  end
end
