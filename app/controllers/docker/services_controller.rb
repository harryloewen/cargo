# frozen_string_literal: true

module Docker
  class ServicesController < ApplicationController
    before_action :set_environment
    before_action :set_service

    def restart
      DockerCli.login(@stack.registry) if @stack.registry?
      DockerCli::Service.update(name: @service.id)

      redirect_to @environment, flash: { notice: t(".notice", name: @service.name) }
    end

  private

    def set_environment
      @environment = Environment.not_deleted.find(params[:environment_id])
      @stack = @environment.stack

      authorize @stack, :show?
      authorize @environment, :logs?
    end

    def set_service
      @service = @environment.docker_services.find { |service| service.id == params[:id] }

      raise ActiveRecord::RecordNotFound if @service.blank?
    end
  end
end
