# frozen_string_literal: true

module Docker
  class TerminalsController < ApplicationController
    before_action :set_environment
    before_action :set_container

    def show; end

  private

    def set_environment
      @environment = Environment.not_deleted.find(params[:environment_id])
      @stack = @environment.stack

      authorize @stack, :show?
      authorize @environment, :terminal?
    end

    def set_container
      @container = @environment.docker_containers.find { |container| container.id == params[:container_id] }

      raise ActiveRecord::RecordNotFound if @container.blank?
    end
  end
end
