# frozen_string_literal: true

class SessionChannel < ApplicationCable::Channel
  def subscribed
    set_environment
    set_container

    @session = TerminalSessionService.new(@container.id, self)
    stream_from @session.stream

    @session.start
  rescue ActiveRecord::RecordNotFound, Pundit::NotAuthorizedError
    reject
  end

  def unsubscribed
    @session&.close
  end

  def write(data)
    return reject_subscription if @session.closed?

    @session.write(data["d"]) if data.is_a?(Hash) && data.key?("d")
  end

  def resize(data)
    return reject_subscription if @session.closed?

    @session.resize(cols: data["cols"].to_i, rows: data["rows"].to_i)
  end

private

  def set_environment
    @environment = Environment.not_deleted.find(params[:environment])

    Pundit.authorize current_user, @environment.stack, :show?
    Pundit.authorize current_user, @environment, :terminal?
  end

  def set_container
    @container = @environment.docker_containers.find { |container| container.id == params[:container] }

    raise ActiveRecord::RecordNotFound if @container.blank?
  end
end
