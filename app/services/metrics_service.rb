# frozen_string_literal: true

class MetricsService
  def initialize(environment, service, options = {})
    @environment = environment
    @service = service
    @end = options[:end].presence || Time.current
    @start = @end.ago(2.hours).in(1.minute).change(sec: 0)
  end

  def requests
    query_range %(sum(round(label_replace(increase(traefik_backend_requests_total{backend=~"#{backend}"}[1m]), "status", "${1}xx", "code", "([0-9]).*"), 0.1)) by (status))
  end

  def response_time
    query_range %(round(sum(rate(traefik_backend_request_duration_seconds_sum{backend=~"#{backend}", protocol!="websocket"}[1m])) / sum(rate(traefik_backend_request_duration_seconds_count{backend=~"#{backend}", protocol!="websocket"}[1m])) * 1000, 0.1))
  end

private

  def query_range(query)
    client.query_range(query: query, start: @start, end: @end, step: 60).body
  end

  def backend
    "backend_#{@environment.docker_name}_#{@service.name}_.+".downcase.tr("_", "-")
  end

  def client
    @client ||= Prometheus.client
  end
end
