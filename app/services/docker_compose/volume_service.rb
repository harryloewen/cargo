# frozen_string_literal: true

module DockerCompose
  class VolumeService
    def initialize(volume_claim:)
      @volume_claim = volume_claim
      @volume = volume_claim.volume
    end

    def service_claim
      [name, target, read_only].compact.join(":")
    end

    def stack_claim
      { name => nil }
    end

  private

    def name
      @volume.name.downcase
    end

    def target
      @volume_claim.target
    end

    def read_only
      @volume_claim.read_only? ? "ro" : nil
    end
  end
end
