# frozen_string_literal: true

class TerminalSessionService
  attr_reader :session_id, :thread

  def initialize(container_id, channel)
    @container = Docker::Container.get(container_id)
    @channel = channel
    @session_id = SecureRandom.hex
    @socket = nil
    @thread = nil
    @instance = create_exec
  end

  def start # rubocop:disable Metrics/MethodLength
    @thread =
      Thread.start do
        start_exec do |socket|
          @socket = socket
          read_socket(socket)
        end

      rescue Docker::Error::TimeoutError
        nil
      ensure
        @channel.send(:reject_subscription)
      end
  end

  def write(string)
    return if closed?

    @socket.write(string)
  end

  def resize(cols:, rows:)
    @instance.resize(h: rows, w: cols)
  end

  def close
    @socket.close_write unless closed?
  end

  def closed?
    return true if @socket.nil?

    @socket.closed?
  end

  def stream
    "session_#{session_id}"
  end

private

  def create_exec
    options = {
      "Container" => @container.id,
      "AttachStdin" => true,
      "AttachStdout" => true,
      "AttachStderr" => true,
      "Tty" => true,
      "Cmd" => [shell]
    }

    Docker::Exec.create(options, @container.connection)
  end

  def start_exec(&block)
    body = {
      "Tty" => true,
      "Detach" => false
    }

    Docker.connection.post("/exec/#{@instance.id}/start", {}, hijack_block: block, body: MultiJson.dump(body))
  end

  def shell
    find_shell("bash") || find_shell("zsh") || find_shell("ash") || find_shell("sh") || "sh"
  end

  def find_shell(name)
    shells.find { |shell| shell.end_with?("/#{name}") }
  end

  def shells
    @shells ||=
      @container
      .exec(["cat", "/etc/shells"], wait: 5)
      .try(:first).try(:first)
      .to_s
      .split("\n")
      .map { |name| name.squish.downcase }
  end

  def read_socket(socket)
    while chuck = socket.readpartial(512) # rubocop:disable Lint/AssignmentInCondition
      ActionCable.server.broadcast(stream, c: chuck.force_encoding("UTF-8"))
    end
  rescue IOError
    nil
  end
end
