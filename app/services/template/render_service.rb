# frozen_string_literal: true

module Template
  class RenderService
    attr_reader :stack, :inputs

    def initialize(file:, stack:, inputs:)
      @template = file.read
      @stack = stack
      @inputs = inputs
    end

    def render
      YAML.safe_load(ERB.new(@template).result(binding)).with_indifferent_access
    end
  end
end
