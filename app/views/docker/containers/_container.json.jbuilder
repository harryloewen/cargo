# frozen_string_literal: true

json.id container.id
json.service_id container.service_id
json.task_id container.task_id
json.image container.image
json.state container.state
json.status container.status
json.ports container.ports
json.created_at container.created_at

json.terminal environment_container_terminal_path(@environment, container.id) if policy(@environment).terminal?
