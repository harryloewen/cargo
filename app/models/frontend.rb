# frozen_string_literal: true

require "ipaddr"
require "resolv"

class Frontend < ApplicationRecord
  attr_encrypted :auth_basic_users, key: Rails.application.key_generator.generate_key("Frontend#auth_basic_users", 32)

  belongs_to :stack, inverse_of: :frontends
  belongs_to :service, inverse_of: :frontends

  scope :ordered, -> { order(Arel.sql('LOWER("frontends"."name") ASC')) }

  attribute :ignore_wrong_dns_entries, default: "0"

  validates :name, presence: true
  validates :name, format: { with: /\A[0-9a-z\-]+\z/i }
  validates :name, uniqueness: { case_sensitive: false, scope: [:stack_id] }
  validates :port, presence: true
  validates :port, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 65_535 }

  validates :redirect, inclusion: { in: [true, false] }
  validates :redirect_regex, presence: true, if: :redirect?
  validates :redirect_replacement, presence: true, if: :redirect?
  validates :redirect_permanent, inclusion: { in: [true, false] }, if: :redirect?

  validates :auth_basic, inclusion: { in: [true, false] }
  validates :auth_basic_remove_header, inclusion: { in: [true, false] }, if: :auth_basic?
  validates :auth_basic_users, presence: true, if: :auth_basic?

  validates :hsts, inclusion: { in: [true, false] }
  validates :hsts_max_age, numericality: { only_integer: true, greater_than_or_equal_to: 0 }, if: :hsts?
  validates :hsts_include_subdomain, inclusion: { in: [true, false] }, if: :hsts?

  validate :validates_service
  validate :validates_hosts_dns_entries

  delegate :name, to: :service, prefix: true

  before_validation :set_stack

  def duplicate
    duplicated_frontend = dup
    duplicated_frontend.name += "-clone"
    duplicated_frontend
  end

private

  def set_stack
    return if stack_id.present?
    return if service.blank?

    self.stack = service.stack
  end

  def validates_service
    return if service&.new_record?

    errors.add(:service, :invalid) unless stack&.services&.include? service
  end

  def validates_hosts_dns_entries # rubocop:disable Metrics/AbcSize
    return if ignore_wrong_dns_entries == "1"
    return if hosts.blank?

    server_ip_addresses = resolve_ip_addresses(Current.settings.host)

    hosts.to_s.split.each do |host|
      next if host.blank?
      next if ip_addresses_match?(server_ip_addresses, resolve_ip_addresses(host))

      errors.add(:hosts, :invalid, message: "is invalid. DNS entry for \"#{host}\" seems to be wrong.")
    end
  end

  def resolve_ip_addresses(host)
    Resolv.getaddresses(host).map { |ip_address| IPAddr.new(ip_address) }
  end

  def ip_addresses_match?(ip_addresses, other_ip_addresses)
    ip_addresses.any? { |ip_address| other_ip_addresses.any? { |other_ip_address| ip_address.include?(other_ip_address) } }
  end
end
