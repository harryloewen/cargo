# frozen_string_literal: true

class ApplicationSetting < ApplicationRecord
  include Colorable

  class << self
    def get
      first || create!
    end
  end

  enum frontend_host_schema: { multiple_subdomains: 0, single_subdomain: 1 }

  attribute :primary_color, default: "#009cff"

  attr_encrypted :prometheus_host, key: Rails.application.key_generator.generate_key("ApplicationSetting#prometheus_host", 32)
  attr_encrypted :minio_access_key, key: Rails.application.key_generator.generate_key("ApplicationSetting#minio_access_key", 32)
  attr_encrypted :minio_secret_key, key: Rails.application.key_generator.generate_key("ApplicationSetting#minio_secret_key", 32)

  validates :default_production_environment_name, presence: true
  validates :default_production_environment_name, format: { with: /\A[0-9a-z\-]+\z/i }

  validates :allow_creation_of_stacks, inclusion: { in: [true, false] }

  validates :host, presence: true
  validates :frontend_host_schema, presence: true
  validates :frontend_host_schema, inclusion: { in: frontend_host_schemas.keys }

  validates :traefik_network, presence: true
  validates :backend_name, presence: true

  validates :prometheus, inclusion: { in: [true, false] }
  validates :prometheus_host, presence: true, if: :prometheus?

  validates :minio, inclusion: { in: [true, false] }
  validates :minio_host, presence: true, if: :minio?
  validates :minio_access_key, presence: true, if: :minio?
  validates :minio_secret_key, presence: true, if: :minio?
  validate :validate_minio_connection, if: :minio?
  validate :validate_node_labels
  validate :validate_default_node_lables

  colorize :primary_color

  def external_services?
    minio?
  end

  def available_node_labels
    @available_node_labels ||= Docker::Node.all.map { |node| node.info.dig("Spec", "Labels")&.keys&.select { |key| key.starts_with?("cargo_") } }.flatten.compact.uniq.map { |label| label.delete_prefix("cargo_") }
  end

  def node_labels=(value)
    super(value&.reject(&:blank?))
  end

  def node_labels
    super || []
  end

  def user_default_approved(email)
    return false if user_default_approved_regex.blank?
    return false unless email.match?(URI::MailTo::EMAIL_REGEXP)

    email.match?(user_default_approved_regex_instance)
  end

  def style
    <<~CSS.squish
      --primary-color: #{primary_color_hsl};
      --primary-color-h: #{primary_color_h};
      --primary-color-s: #{primary_color_s};
      --primary-color-l: #{primary_color_l};
    CSS
  end

private

  def user_default_approved_regex_instance
    Regexp.new(user_default_approved_regex, Regexp::IGNORECASE)
  end

  def validate_minio_connection
    return unless minio_host? && minio_access_key? && minio_secret_key?

    message = ExternalServices::MinioService.new(self).try_connection
    return if message == "success"

    errors.add(:minio, message || "Unknown error")
  end

  def validate_node_labels
    return if node_labels.blank?

    invalid_node_labels = node_labels.select { |node_label| available_node_labels.exclude?(node_label) }
    errors.add(:node_labels, :invalid, message: "#{invalid_node_labels.to_sentence} are not available") if invalid_node_labels.any?
  end

  def validate_default_node_lables
    errors.add(:default_production_node_label, :inclusion) if default_production_node_label? && node_labels.exclude?(default_production_node_label)
    errors.add(:default_review_node_label, :inclusion) if default_review_node_label? && node_labels.exclude?(default_review_node_label)
  end
end
