# frozen_string_literal: true

class VolumeClaim < ApplicationRecord
  belongs_to :service
  belongs_to :volume

  scope :ordered_by_volume, -> { joins(:volume).merge(Volume.ordered) }
  scope :ordered_by_service, -> { joins(:service).merge(Service.ordered) }

  validates :volume_id, uniqueness: { scope: :service_id }
  validates :target, presence: true
  validates :target, uniqueness: { case_sensitive: false, scope: :service_id }
  validates :read_only, inclusion: { in: [true, false] }

  def stack
    service&.stack || volume&.stack
  end
end
