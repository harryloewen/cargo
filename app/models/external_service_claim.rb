# frozen_string_literal: true

class ExternalServiceClaim < ApplicationRecord
  belongs_to :external_service
  belongs_to :service

  validates :external_service_id, uniqueness: { case_sensitive: false, scope: :service_id }, if: :will_save_change_to_external_service_id?
  validates :service_id, uniqueness: { case_sensitive: false, scope: :external_service_id }, if: :will_save_change_to_service_id?
  validates :target, presence: true
  validates :target, uniqueness: { case_sensitive: false, scope: :service_id }, if: :will_save_change_to_target?

  def stack
    service&.stack || external_service&.stack
  end
end
