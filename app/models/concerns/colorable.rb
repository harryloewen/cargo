# frozen_string_literal: true

module Colorable
  extend ActiveSupport::Concern

  class_methods do
    def colorize(attribute)
      validates attribute, presence: true, format: /\A#[a-f0-9]{6}\z/i

      define_method("#{attribute}_hsl") { colorable_attribute_hsl(attribute) }
      define_method("#{attribute}_h") { colorable_attribute_h(attribute) }
      define_method("#{attribute}_s") { colorable_attribute_s(attribute) }
      define_method("#{attribute}_l") { colorable_attribute_l(attribute) }
    end
  end

private

  def colorable_attribute_color_rgb(attribute)
    Color::RGB.by_hex(public_send(attribute))
  end

  def colorable_attribute_color_hsl(attribute)
    colorable_attribute_color_rgb(attribute).to_hsl
  end

  def colorable_attribute_hsl(attribute)
    colorable_attribute_color_hsl(attribute).css_hsl
  end

  def colorable_attribute_h(attribute)
    format "%3.2f", colorable_attribute_color_hsl(attribute).hue # rubocop:disable Style/FormatStringToken
  end

  def colorable_attribute_s(attribute)
    format "%3.2f%%", colorable_attribute_color_hsl(attribute).saturation # rubocop:disable Style/FormatStringToken
  end

  def colorable_attribute_l(attribute)
    format "%3.2f%%", colorable_attribute_color_hsl(attribute).luminosity # rubocop:disable Style/FormatStringToken
  end
end
