# frozen_string_literal: true

module ApplicationHelper
  def alert_style_class(type)
    case type.to_sym
    when :notice
      "alert-info"
    when :success
      "alert-success"
    else
      "alert-danger"
    end
  end

  def shallow(parent, child)
    child.try(:new_record?) ? [parent, child] : child
  end

  def page_heading(&block)
    content_for :page_heading, &block
  end

  def number_to_duration(number)
    ActiveSupport::Duration.build(number).parts.map do |type, value|
      "#{value.to_i}#{type[0]}"
    end.join(" ")
  end

  def remote_switch_tag(model, field, disabled: false)
    simple_form_for(model, remote: true, html: { disabled: disabled, class: "form-inline justify-content-center" }) do |f|
      f.input field, input_html: { id: "#{model.model_name.param_key}_#{field}_#{model.id}", disabled: disabled, onchange: "Rails.fire(this.form, 'submit')" }, wrapper: :custom_boolean_switch, label: "&nbsp;".html_safe, wrapper_html: { class: "pl-3 mb-0" }
    end
  end
end
