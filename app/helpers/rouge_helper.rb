# frozen_string_literal: true

module RougeHelper
  def rouge(source = nil, options = {}, &block) # rubocop:disable Metrics/MethodLength, Metrics/AbcSize
    if block_given?
      options = source
      source = capture(&block)
    end
    raise ArgumentError, "source is missing" if source.nil?

    formatter = Rouge::Formatters::HTML.new
    formatter = Rouge::Formatters::HTMLLinewise.new(formatter, class: "line-%i")
    lexer = Rouge::Lexer.find(options.delete(:language))

    options[:class] = "highlight mb-0 #{options[:class]}".squish

    content_tag :pre, options do
      formatter.format(lexer.lex(source)).html_safe # rubocop:disable Rails/OutputSafety
    end
  end
end
