# frozen_string_literal: true

module ClipboardHelper
  def copy_input(value)
    content_tag :div, class: "input-group input-group-sm mb-2", "data-controller": "clipboard" do
      concat content_tag :input, "", class: "form-control", "data-target": "clipboard.source", type: "text", value: value, readonly: true
      concat(content_tag(:div, class: "input-group-append") do
        content_tag :button, "Copy", class: "btn btn-secondary clipboard-button", "data-action": "clipboard#copy"
      end)
    end
  end
end
