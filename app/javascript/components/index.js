import TurbolinksAdapter from "vue-turbolinks"
import Vue from "vue"

import "./filters"

Vue.use(TurbolinksAdapter)

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => {
  const component = key.split('/').splice(1).join("-").split(`.`)[0]

  Vue.component(component, () => import(`${key}`))
})

document.addEventListener("turbolinks:load", () => {
  if (!document.getElementById("vue-app")) return;

  new Vue({
    el: "#vue-app"
  })
})
