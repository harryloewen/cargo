import Vue from "vue"
import moment from 'moment'

Vue.filter('formatId', (value) => {
  return value.toString().substr(0, 12)
})

Vue.filter('formatImage', (value) => {
  let [name, tag] = value.toString().split(":")

  if (name.length > 12) {
    let elements = name.split("/")
    if (elements.length > 2) {
      name = elements.slice(1).join("/")
    }
  }
  if (tag.length > 12) {
    tag = `${tag.substr(0, 3)}..${tag.substr(-3, 3)}`
  }

  return `${name}:${tag}`
})

Vue.filter('fromNow', (value) => {
  return moment(value).fromNow()
})
