import { Controller } from "stimulus"
import { Terminal } from 'xterm'
import { FitAddon } from 'xterm-addon-fit';
import consumer from "../channels/consumer"

export default class extends Controller {
  static targets = [ "terminal", "indicator", "alert" ]

  initialize() {
    this.connected = false

    this.term = new Terminal()
    this.fitAddon = new FitAddon()
    this.term.loadAddon(this.fitAddon)

    this.term.onData(data => { this.sendData(data) })
    this.term.onResize(data => { this.resize(data) })
    this.term.open(this.terminalTarget)

    this.sessionConnect()

    document.addEventListener('turbolinks:before-visit', this.confirm)
    window.addEventListener('beforeunload', this.confirm)
  }

  disconnect() {
    this.session.unsubscribe()
    this.sessionClosed()

    let alert = document.createElement("p")
    alert.textContent = "This terminal session was closed! Please refresh the page to start a new session!"
    alert.classList.add("text-muted")
    this.element.parentNode.appendChild(alert)
    this.element.remove()
  }

  sendData(data) {
    if (this.connected == false) return;

    this.session.perform("write", { d: data })
  }

  resize(data) {
    if (this.connected == false) return;

    this.session.perform("resize", data)
  }

  sessionConnect() {
    let environment = this.element.getAttribute("data-environment")
    let container = this.element.getAttribute("data-container")

    this.session = consumer.subscriptions.create({
      channel: "SessionChannel",
      environment: environment,
      container: container
    }, {
      connected: () => { this.sessionConnected() },
      disconnected: () => { this.sessionDisconnected() },
      rejected: () => { this.sessionRejected() },
      received: (data) => { this.sessionReceived(data) }
    })
  }

  sessionConnected() {
    this.connected = true
    this.indicatorTarget.classList.add("connected")
    this.fitAddon.fit()
  }

  sessionDisconnected() {
    this.sessionClosed()
  }

  sessionRejected() {
    this.sessionClosed()
  }

  sessionClosed(){
    if (this.connected == false) return;

    document.removeEventListener('turbolinks:before-visit', this.confirm)
    window.removeEventListener('beforeunload', this.confirm)

    this.connected = false
    this.indicatorTarget.classList.remove("connected")
    this.indicatorTarget.classList.add("disconnected")
    this.alertTarget.classList.add("d-none")
    this.term.writeln("")
    this.term.writeln("Session closed")
  }

  sessionReceived(data) {
    this.term.write(data["c"])
  }

  confirm(event) {
    if (event.type == "turbolinks:before-visit") {
      if (!confirm("Your terminal session is still open, please close it before switching to another page.")) {
        event.preventDefault()
        event.stopPropagation()
      }
    } else {
      event.returnValue = "Your terminal session is still open, please close it before switching to another page.";
      return event.returnValue
    }
  }
}
