import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [ "input" ]

  initialize() {
    this.codeMirror = CodeMirror.fromTextArea(this.inputTarget, {
      lineNumbers: true,
      matchBrackets: true,
      tabSize: 2
    })

    this.languageElement = document.getElementById("config_language")
    this.languageElement.addEventListener("change", () => this.changeLanguage())
  }

  connect() {
    this.changeLanguage()
  }

  changeLanguage() {
    this.codeMirror.setOption("mode", this.languageOption())
  }

  language() {
    return this.languageElement.options[this.languageElement.selectedIndex].value || "text"
  }

  languageOption(){
    switch(this.language()) {
      case "html":
        return { name: "htmlmixed" }
      case "json":
        return "application/json"
      case "markdown":
        return { name: "gfm" }
      case "text":
        return "text/plain"
      default:
        return this.language()
    }
  }
}
