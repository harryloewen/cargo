# frozen_string_literal: true

class CreateVolumes < ActiveRecord::Migration[5.2]
  def change
    create_table :volumes, id: :uuid do |t|
      t.references :stack, foreign_key: true, null: false, type: :uuid
      t.string :name, null: false

      t.index %i[stack_id name], unique: true

      t.timestamps
    end
  end
end
