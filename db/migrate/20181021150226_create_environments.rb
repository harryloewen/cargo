# frozen_string_literal: true

class CreateEnvironments < ActiveRecord::Migration[5.2]
  def change
    create_table :environments, id: :uuid do |t|
      t.references :stack, foreign_key: true, null: false, type: :uuid
      t.string :name, null: false
      t.datetime :deployed_at
      t.datetime :deleted_at

      t.index %i[stack_id name deleted_at], unique: true

      t.timestamps
    end
  end
end
