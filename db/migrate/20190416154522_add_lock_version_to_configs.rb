# frozen_string_literal: true

class AddLockVersionToConfigs < ActiveRecord::Migration[5.2]
  def change
    add_column :configs, :lock_version, :integer, default: 0, null: false

    Config.find_each do |config|
      config.save!
    rescue StandardError => exception
      say "Config##{config.id}: #{exception.message}"
    end
  end
end
