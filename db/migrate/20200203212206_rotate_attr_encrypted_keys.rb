# frozen_string_literal: true

class RotateAttrEncryptedKeys < ActiveRecord::Migration[6.0]
  class BaseRecord < ActiveRecord::Base # rubocop:disable Rails/ApplicationRecord
    self.abstract_class = true

    def rotate_keys!
      encrypted_attributes.each_key do |attribute|
        public_send("#{attribute}=", public_send(attribute))
      end

      save!(touch: false)
    end

    def decrypting?(attribute)
      encrypted_attributes[attribute.to_sym][:operation] == :decrypting
    end

    def key(klass, attribute)
      if decrypting?(attribute)
        attribute = "#{attribute}_key" unless attribute.end_with?("_key")

        Cargo.config.public_send("#{klass}_#{attribute}".underscore)
      else
        Rails.application.key_generator.generate_key("#{klass}##{attribute}", 32)
      end
    end
  end

  class ApplicationSetting < BaseRecord
    attr_encrypted :prometheus_host, key: ->(object) { object.key("ApplicationSetting", "prometheus_host") }
    attr_encrypted :minio_access_key, key: ->(object) { object.key("ApplicationSetting", "minio_access_key") }
    attr_encrypted :minio_secret_key, key: ->(object) { object.key("ApplicationSetting", "minio_secret_key") }
  end

  class Config < BaseRecord
    attr_encrypted :value, key: ->(object) { object.key("Config", "value") }
    attr_encrypted :review_value, key: ->(object) { object.key("Config", "review_value") }
  end

  class Deployment < BaseRecord
    attr_encrypted :template, key: ->(object) { object.key("Deployment", "template") }, allow_empty_value: true, marshal: true
  end

  class ExternalServiceEnvironmentClaim < BaseRecord
    attr_encrypted :settings, key: ->(object) { object.key("ExternalServiceEnvironmentClaim", "settings") }, marshal: true
  end

  class Frontend < BaseRecord
    attr_encrypted :auth_basic_users, key: ->(object) { object.key("Frontend", "auth_basic_users") }
  end

  class Service < BaseRecord
    attr_encrypted :healthcheck_test, key: ->(object) { object.key("Service", "healthcheck_test") }
  end

  class Stack < BaseRecord
    attr_encrypted :registry_password, key: ->(object) { object.key("Stack", "registry_password") }
  end

  def up
    [ApplicationSetting, Config, Deployment, ExternalServiceEnvironmentClaim, Frontend, Service, Stack].each do |klass|
      say_with_time "Rotating #{klass.to_s.demodulize} keys" do
        klass.find_each(&:rotate_keys!)
      end
    end
  end

  def down; end
end
