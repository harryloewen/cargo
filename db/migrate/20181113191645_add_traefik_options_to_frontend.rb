# frozen_string_literal: true

class AddTraefikOptionsToFrontend < ActiveRecord::Migration[5.2]
  def change
    change_table :frontends, bulk: true do |t|
      t.boolean :redirect, default: false
      t.string :redirect_regex
      t.string :redirect_replacement
      t.boolean :redirect_permanent, default: true

      t.boolean :auth_basic, default: false
      t.boolean :auth_basic_remove_header, default: true
      t.text :encrypted_auth_basic_users
      t.string :encrypted_auth_basic_users_iv

      t.text :request_headers
      t.text :response_headers

      t.boolean :hsts, default: true
      t.integer :hsts_max_age, default: 180.days
      t.boolean :hsts_include_subdomain, default: false
    end
  end
end
