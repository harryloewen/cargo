# frozen_string_literal: true

class MakesFrontendHostsOptional < ActiveRecord::Migration[5.2]
  def change
    change_column_null :frontends, :hosts, null: true
  end
end
