# frozen_string_literal: true

class RenameUsersGitlabUidToOidcUidAndGitlabTokenToOidcToken < ActiveRecord::Migration[6.0]
  def change
    change_table :users, bulk: true do |t|
      t.rename :gitlab_uid, :oidc_uid
      t.rename :gitlab_token, :oidc_token
    end
  end
end
