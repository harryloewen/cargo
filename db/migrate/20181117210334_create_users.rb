# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users, id: :uuid do |t|
      t.string :username, null: false
      t.string :gitlab_uid, null: false
      t.string :gitlab_token, null: false
      t.boolean :admin, default: false, null: false

      t.timestamps

      t.index :username, unique: true
      t.index :gitlab_uid, unique: true
      t.index :gitlab_token, unique: true
    end
  end
end
