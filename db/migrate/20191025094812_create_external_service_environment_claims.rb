# frozen_string_literal: true

class CreateExternalServiceEnvironmentClaims < ActiveRecord::Migration[6.0]
  def change
    create_table :external_service_environment_claims, id: :uuid do |t|
      t.references :external_service, foreign_key: true, type: :uuid, index: { name: "index_external_service_environment_claims_on_e_s_id" }
      t.references :environment, null: false, foreign_key: true, type: :uuid
      t.text :encrypted_settings
      t.string :encrypted_settings_iv

      t.index :encrypted_settings_iv, unique: true, name: "index_external_service_environment_claims_on_e_s_iv"
      t.index %i[external_service_id environment_id], unique: true, name: "index_external_service_environment_claims_on_e_s_id_and_e_id"

      t.timestamps
    end
  end
end
