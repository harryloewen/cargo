# frozen_string_literal: true

class CreateStacks < ActiveRecord::Migration[5.2]
  def change
    enable_extension "pgcrypto"

    create_table :stacks, id: :uuid do |t|
      t.string :name, null: false

      t.index :name, unique: true

      t.timestamps
    end
  end
end
