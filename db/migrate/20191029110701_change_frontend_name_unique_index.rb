# frozen_string_literal: true

class ChangeFrontendNameUniqueIndex < ActiveRecord::Migration[6.0]
  def change
    remove_index :frontends, %i[service_id name]
    add_index :frontends, %i[stack_id name], unique: true
  end
end
