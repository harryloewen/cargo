# frozen_string_literal: true

class CreateApplicationSettings < ActiveRecord::Migration[6.0]
  def change
    create_table :application_settings, id: :uuid do |t|
      t.boolean :allow_creation_of_stacks, default: true, null: false
      t.string :default_production_environment_name, default: "production", null: false
      t.string :user_default_approved_regex

      t.timestamps
    end
  end
end
