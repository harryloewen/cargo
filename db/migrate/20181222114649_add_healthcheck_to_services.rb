# frozen_string_literal: true

class AddHealthcheckToServices < ActiveRecord::Migration[5.2]
  def up
    change_table :services, bulk: true do |t|
      t.boolean :healthcheck, default: false, null: false
      t.text :encrypted_healthcheck_test
      t.string :encrypted_healthcheck_test_iv
      t.integer :healthcheck_interval, null: false, default: 30
      t.integer :healthcheck_timeout, null: false, default: 30
      t.integer :healthcheck_start_period, null: false, default: 0
      t.integer :healthcheck_retries, null: false, default: 3
    end
  end

  def down
    change_table :services, bulk: true do |t|
      t.remove :healthcheck
      t.remove :encrypted_healthcheck_test
      t.remove :encrypted_healthcheck_test_iv
      t.remove :healthcheck_interval
      t.remove :healthcheck_timeout
      t.remove :healthcheck_start_period
      t.remove :healthcheck_retries
    end
  end
end
