# frozen_string_literal: true

class CreateVolumeClaims < ActiveRecord::Migration[5.2]
  def change
    create_table :volume_claims, id: :uuid do |t|
      t.references :service, foreign_key: true, null: false, type: :uuid
      t.references :volume, foreign_key: true, null: false, type: :uuid
      t.string :target, null: false
      t.boolean :read_only, default: true, null: false

      t.index %i[service_id volume_id], unique: true
      t.index %i[service_id target], unique: true

      t.timestamps
    end
  end
end
